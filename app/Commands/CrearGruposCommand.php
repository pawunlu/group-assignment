<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CrearGruposCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'make:groups
                                {groups : Archivo con lista de nombres. Un nombre por linea.}
                                {--group-size=2 : La cantidad de integrantes del grupo}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Crea grupos a partir de un listado de nombres';

    /**
     * Levanta los estudiantes de un archivo y construye una lista con
     *  los mismos
     */
    private function getElements()
    {
        $filePath = $this->argument('groups');
        $fh = fopen($filePath, 'r');
        $estudiantes = [];
        while ($line = fgets($fh)) {
            $estudiantes[] = trim($line);
        }
        $this->info('Cantidad de estudiantes: ' . count($estudiantes));
        return $estudiantes;
    }

    /**
     * Genera los grupos a partir de una lista de nombres.
     */
    private function makeGroups($estudiantes, $size = 2)
    {
        // Si len $estudiantes < size, es 1 solo grupo
        // Sino, armar grupos
        if (count($estudiantes) <= $size) {
            return $groups = [1 => $estudiantes];
        }

        $grupos = [];
        $estudiantes_libres = $estudiantes;
        while (!empty($estudiantes_libres)) {
            // $size*2 < count($estudiantes_libres)
            //  Multiplicar por 2 garantiza que el ultimo grupo no va a quedar
            //   alguien solo, sino que sera incluido en el grupo anterior.
            //  Ejemplo: 5 Estudiantes, grupos de 2.
            //   No queremos 3 grupos de 2, 2 y 1, sino 2 de 2 y 3.
            if ($size*2 <= count($estudiantes_libres)) {
                $nuevo_grupo = array_rand($estudiantes_libres, $size);
            } else {
                $nuevo_grupo = array_keys($estudiantes_libres);
            }
            $miembros_grupo = [];
            foreach ($nuevo_grupo as $miembro) {
                $miembros_grupo[] = $estudiantes_libres[$miembro];
                unset($estudiantes_libres[$miembro]);
            }
            $grupos[] = $miembros_grupo;
        }
        return $grupos;
    }

    private function mostrarGrupos($grupos)
    {
        $this->info("Grupos generados:");
        $this->info("|");
        foreach ($grupos as $grupo_id => $grupo) {
            $this->info("|");
            $this->info("| -> Grupo $grupo_id:");
            $this->info("|");
            foreach ($grupo as $miembro) {
                $this->info("| - ->   $miembro");
            }
            $this->info("|");
            $this->info("|");
        }
        $this->info("X");
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $estudiantes = $this->getElements();
        $grupos = $this->makeGroups($estudiantes, $this->option('group-size'));
        $this->mostrarGrupos($grupos);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
