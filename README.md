# Asignador de Grupos

Proyecto creado para armar grupos de estudiantes de la materia PAW - UNLu

* Este proyecto fue armado con la libreria [Laravel Zero](https://laravel-zero.com/)

## Uso

```
./group-assignment make:groups archivo_estudiantes.txt
```

Donde:

* `archivo_estudiantes.txt`: Listado de nombres de estudiantes. (Un nombre por linea)

## Instalación

Descargar el [archivo compilado](https://gitlab.com/pawunlu/group-assignment/raw/53fd7f43c830fe9107e93dfff4a53df0e0d89735/builds/group-assignment).

Darle permisos de ejecución: `chmod a+x group-assignment`

Luego ejecutar:

```
$ ./group-assignment -v
  Group-assignment  1.1.1
  USAGE: group-assignment <command> [options] [arguments]
  make:groups Crea grupos a partir de un listado de nombres
```

## Desarrollo

* Clonar el proyecto
* Desarrollar cambios
* Armar la build con `php group-assignment app:build`.

